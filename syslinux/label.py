class Label():

	def __init__(self, idstr, **kwargs):
		self.idstr = idstr
		self.menu_label = None
		self.kernel = None
		self.initrd = None
		self.append = None
		self.localboot = False
		for k in kwargs.keys():
			v = kwargs[k]
			if k == 'menu_label':
				self.menu_label = v
			if k == 'kernel':
				self.kernel = v
			if k == 'initrd':
				self.initrd = v
			if k == 'append':
				self.append = v
			if k == 'localboot':
				self.localboot = v

	def __str__(self):
		s = "LABEL %s" % self.idstr
		if self.menu_label:
			s += "\n\tMENU LABEL %s" % self.menu_label
		if self.kernel:
			s += "\n\tKERNEL\t%s" % self.kernel
		if self.initrd:
			s += "\n\tINITRD\t%s" % self.initrd
		if self.append:
			s += "\n\tAPPEND\t%s" % self.append
		if self.localboot:
			s += "\n\tLOCALBOOT"
		return s
