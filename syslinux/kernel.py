class KernelBase():

	DIRECTIVE = ""

	def __init__(self, path):
		self.path = path

	def __str__(self):
		return "%s %s" % (self.DIRECTIVE, self.path)

class Kernel(KernelBase):

	DIRECTIVE = "KERNEL"

class Linux(KernelBase):

	DIRECTIVE = "LINUX"

class Initrd(KernelBase):

	DIRECTIVE = "INITRD"

class Boot(KernelBase):

	DIRECTIVE = "BOOT"

class Bss(KernelBase):

	DIRECTIVE = "BSS"

class Pxe(KernelBase):

	DIRECTIVE = "PXE"

class FdImage(KernelBase):

	DIRECTIVE = "FDIMAGE"

class ComBoot(KernelBase):

	DIRECTIVE = "COMBOOT"

class Com32(KernelBase):

	DIRECTIVE = "COM32"

class Config(KernelBase):

	DIRECTIVE = "CONFIG"

class LocalBoot():

	def __init__(self, kind):
		self.kind = kind

	def __str__(self):
		return "LOCALBOOT %d" % self.kind
