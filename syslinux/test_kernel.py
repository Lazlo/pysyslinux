import unittest
import syslinux

class KernelTestCase(unittest.TestCase):

	def testCreateKernel(self):
		expected = "KERNEL linux"
		actual = str(syslinux.Kernel("linux"))
		self.assertEqual(actual, expected)

	def testCreateLinux(self):
		expected = "LINUX vmlinuz"
		actual = str(syslinux.Linux("vmlinuz"))
		self.assertEqual(actual, expected)

	def testCreateInitrd(self):
		expected = "INITRD initrd.gz"
		actual = str(syslinux.Initrd("initrd.gz"))
		self.assertEqual(actual, expected)

	def testCreateBoot(self):
		expected = "BOOT fw.bin"
		actual = str(syslinux.Boot("fw.bin"))
		self.assertEqual(actual, expected)

	def testCreateBss(self):
		expected = "BSS out.bss"
		actual = str(syslinux.Bss("out.bss"))
		self.assertEqual(actual, expected)

	def testCreatePxe(self):
		expected = "PXE pxelinux.0"
		actual = str(syslinux.Pxe("pxelinux.0"))
		self.assertEqual(actual, expected)

	def testCreateFdimage(self):
		expected = "FDIMAGE image.img"
		actual = str(syslinux.FdImage("image.img"))
		self.assertEqual(actual, expected)

	def testCreateComBoot(self):
		expected = "COMBOOT image.com"
		actual = str(syslinux.ComBoot("image.com"))
		self.assertEqual(actual, expected)

	def testCreateCom32(self):
		expected = "COM32 program.c32"
		actual = str(syslinux.Com32("program.c32"))
		self.assertEqual(actual, expected)

	def testCreateConfig(self):
		expected = "CONFIG example.cfg"
		actual = str(syslinux.Config("example.cfg"))
		self.assertEqual(actual, expected)

	def testCreateLocalBoot(self):
		expected = "LOCALBOOT 0"
		actual = str(syslinux.LocalBoot(0))
		self.assertEqual(actual, expected)
