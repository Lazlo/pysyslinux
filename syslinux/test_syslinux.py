import unittest
import syslinux

class SysLinuxTestCase(unittest.TestCase):

	def testCreateComment(self):
		expected = "# Important comment"
		actual = str(syslinux.Comment("Important comment"))
		self.assertEqual(actual, expected)

	def testCreateMenu(self):
		expected = "MENU Some string describing the menu"
		actual = str(syslinux.Menu("Some string describing the menu"))
		self.assertEqual(actual, expected)

	def testCreateUi(self):
		expected = "UI menu.c32"
		actual = str(syslinux.Ui('menu.c32'))
		self.assertEqual(actual, expected)

	def testCreateDefault(self):
		expected = "DEFAULT linux"
		actual = str(syslinux.Default("linux"))
		self.assertEqual(actual, expected)

	def testCreateTimeout(self):
		expected = "TIMEOUT 30"
		actual = str(syslinux.Timeout(30))
		self.assertEqual(actual, expected)

	def testCreateSerial(self):
		expected = "SERIAL 0 115200 0"
		actual = str(syslinux.Serial(0, baudrate=115200, flowcontrol=0))
		self.assertEqual(actual, expected)

	def testCreateConsole(self):
		expected = "CONSOLE 1"
		actual = str(syslinux.Console(1))
		self.assertEqual(actual, expected)

	def testCreateKbdmap(self):
		expected = "KBDMAP de"
		actual = str(syslinux.Kbdmap('de'))
		self.assertEqual(actual, expected)

	def testCreateInclude(self):
		expected = "INCLUDE somefile.txt"
		actual = str(syslinux.Include("somefile.txt"))
		self.assertEqual(actual, expected)

	def testCreateLabel(self):
		expected = "LABEL some-boot-option"
		actual = str(syslinux.Label("some-boot-option"))
		self.assertEqual(actual, expected)

	def testCreateLabelWithMenuLabel(self):
		expected = "LABEL foobar\n\tMENU LABEL FooBar"
		actual = str(syslinux.Label("foobar", menu_label='FooBar'))
		self.assertEqual(actual, expected)

	def testCreateLabelWithLocalboot(self):
		expected = "LABEL boot-local\n\tLOCALBOOT"
		actual = str(syslinux.Label("boot-local", localboot=True))
		self.assertEqual(actual, expected)

	def testCreateLabelWithKernel(self):
		expected = "LABEL recovery\n\tKERNEL\tlinux"
		actual = str(syslinux.Label("recovery", kernel="linux"))
		self.assertEqual(actual, expected)

	def testCreateLabelWithInitrd(self):
		expected = "LABEL installer\n\tINITRD\tinitrd.gz"
		actual = str(syslinux.Label("installer", initrd="initrd.gz"))
		self.assertEqual(actual, expected)

	def testCreateLabelWithAppend(self):
		expected = "LABEL quickboot\n\tAPPEND\tnet.ifnames=0"
		actual = str(syslinux.Label("quickboot", append='net.ifnames=0'))
		self.assertEqual(actual, expected)

	def testCreateLabelWithAllArgs(self):
		expected = "LABEL debian\n\tMENU LABEL Debian\n\tKERNEL\tlinux\n\tINITRD\tinitrd.gz"
		actual = str(syslinux.Label("debian", menu_label="Debian", kernel="linux", initrd="initrd.gz"))
		self.assertEqual(actual, expected)
