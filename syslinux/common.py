class Comment():

	def __init__(self, txt):
		self.txt = txt

	def __str__(self):
		return "# %s" % self.txt

class Menu():

	def __init__(self, txt):
		self.txt = txt

	def __str__(self):
		return "MENU %s" % self.txt

class Ui():

	def __init__(self, module):
		self.module = module

	def __str__(self):
		return "UI %s" % self.module

class Default():

	def __init__(self, idstr):
		self.idstr = idstr

	def __str__(self):
		return "DEFAULT %s" % self.idstr

class Timeout():

	def __init__(self, deci_sec):
		self.deci_sec = deci_sec

	def __str__(self):
		return "TIMEOUT %d" % self.deci_sec

class Serial():

	def __init__(self, port, baudrate=9600, flowcontrol=0):
		self.port = port
		self.baudrate = baudrate
		self.flowcontrol = flowcontrol

	def __str__(self):
		return "SERIAL %d %d %d" % (self.port, self.baudrate, self.flowcontrol)

class Console():

	def __init__(self, flag_val):
		self.flag_val = flag_val

	def __str__(self):
		return "CONSOLE %d" % self.flag_val

class Kbdmap():

	def __init__(self, keymap):
		self.keymap = keymap

	def __str__(self):
		return "KBDMAP %s" % self.keymap

class Include():

	def __init__(self, path):
		self.path = path

	def __str__(self):
		return "INCLUDE %s" % self.path
