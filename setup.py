import setuptools

with open('README.md', 'r') as fh:
	long_description = fh.read()

setuptools.setup(
	name='pysyslinux',
	version='0.1',
	scripts=[],
	author="Daniel Laszlo Sitzer",
	author_email="dlsitzer@gmail.com",
	description="...",
	long_description=long_description,
	long_description_content_type='text/markdown',
	url='https://gitlab.com/Lazlo/pysyslinux',
	packages=setuptools.find_packages(),
	classifiers=[
		"Programming Languate :: Python :: 3",
	],
)
