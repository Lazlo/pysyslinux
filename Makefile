default: all
all: test

TEST_CMD = python3 -m xmlrunner discover -s syslinux
VTEST_CMD = $(TEST_CMD) -v
PKG_CMD = python3 setup.py bdist_wheel sdist

test:
	$(TEST_CMD)
vtest:
	$(VTEST_CMD)
package:
	$(PKG_CMD)
clean:
	$(RM) -r */__pycache__
	$(RM) *.xml
	$(RM) -r build
	$(RM) -r dist
	$(RM) -r pysyslinux.egg-info
