#!/bin/bash

pkgs=""
pkgs="$pkgs make"
pkgs="$pkgs python3 python3-pip python3-setuptools"

apt-get -q install -y $pkgs
pip3 install --system -r requirements.txt
