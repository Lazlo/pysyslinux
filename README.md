# pySyslinux

Create [SYSLINUX bootloader](https://www.syslinux.org/wiki/index.php/The_Syslinux_Project) configuration using Python 3.

## Usage

Install package from ```*.whl``` file like this

```
pip3 install pysyslinux-0.1-py3-none-any.whl
```

Run ```python3``` and use the package like this

```
from syslinux import Default, Timeout, Label
s = "%s\n" % Default('menu.c32')
s += "%s\n" % Timeout(30)
s += "%s" % Label('localboot', localboot=True)
print(s)
```

This will produce the following:

```
DEFAULT menu.c32
TIMEOUT 30
LABEL localboot
	LOCALBOOT
```

## Development

Before testing, make sure to run `setup.sh` to install dependencies
needed during development and testing.

To execute the tests, run ```make vtest``` or just ```make```.

### Package Building

Create .whl and .tar distribution packages in the ```dist/``` directory:

```
make package
```
